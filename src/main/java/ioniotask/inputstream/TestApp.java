package ioniotask.inputstream;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

public class TestApp {
    private static CustomInputStream testStream;

    public static void main(String[] args) {
        int pushbacksize = 10;
        byte[] buf = new byte[15];
        try {
            testStream = new CustomInputStream(new FileInputStream("output.txt"), pushbacksize);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        try {
            testStream.read(buf);
        } catch (IOException e) {
            e.printStackTrace();
        }
        for (int k = 0; k < buf.length; k++) {
            System.out.print((char) buf[k]);
        }
        try {
            testStream.unread(pushbacksize);
            testStream.read(buf);
        } catch (IOException e) {
            e.printStackTrace();
        }
        for (int k = 0; k < buf.length; k++) {
            System.out.print((char) buf[k]);
        }
    }
}
