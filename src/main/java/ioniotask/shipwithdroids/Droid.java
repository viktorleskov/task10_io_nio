package ioniotask.shipwithdroids;

import java.io.Serializable;

public class Droid implements Serializable {
    private int number;
    private transient String name;

    public Droid(int number, String name) {
        this.number = number;
        this.name = name;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public int getNumber() {
        return number;
    }
    public String getName(){
        return name;
    }
}
