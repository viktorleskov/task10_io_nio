package ioniotask.shipwithdroids;

import java.io.*;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

public class Ship implements Serializable {
    private static ObjectOutputStream outputStream;
    private static ObjectInputStream inputStream;
    private static String DEFAULT_FILE_NAME = "droids.dat";
    Droid[] droids;

    public Ship(Droid... droids) {
        this.droids = droids;
    }

    public Droid[] getDroids() {
        return droids;
    }

    public static void main(String[] args) throws IOException, ClassNotFoundException {
        Ship forSerialize = new Ship(
                new Droid(new Random().nextInt(20), "Droid1"),
                new Droid(new Random().nextInt(20), "Droid2"),
                new Droid(new Random().nextInt(20), "Droid3"),
                new Droid(new Random().nextInt(20), "Droid4"),
                new Droid(new Random().nextInt(20), "Droid5"),
                new Droid(new Random().nextInt(20), "Droid6")
        );
        outputStream = new ObjectOutputStream(new FileOutputStream(DEFAULT_FILE_NAME));
        List<Droid> objectList = new LinkedList<>(Arrays.asList(forSerialize.getDroids()));
        for (Droid droid : objectList) {
            System.out.println(droid.getNumber() + " " + droid.getName());
        }
        for (Droid droid : objectList) {
            outputStream.writeObject(droid);
        }
        objectList = new LinkedList<>();
        outputStream.close();
        inputStream = new ObjectInputStream(new FileInputStream(DEFAULT_FILE_NAME));
        objectList.add((Droid) inputStream.readObject());
        objectList.forEach(droid -> System.out.print(droid.getNumber() + " " + droid.getName()));
    }
}
