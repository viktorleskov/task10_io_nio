package ioniotask.performancecompare;

import java.util.Random;

public abstract class StringGenerator {
    private StringGenerator(){}
    public static String getRandomString(int length){
        StringBuilder buf = new StringBuilder();
        for(int k=0; k<length; k++){
            buf.append((char)(new Random().nextInt(15)+65));
        }
        buf.append("\n");
        return buf.toString();
    }
}
