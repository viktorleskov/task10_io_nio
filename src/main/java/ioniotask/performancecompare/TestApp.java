package ioniotask.performancecompare;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.nio.CharBuffer;

public class TestApp {
    private static Logger LOG = LogManager.getLogger(TestApp.class);
    private static int DEFAULT_BUFFER_SIZE = 1024;
    private static String DEFAULT_FILE_NAME = "output.txt";
    private static int DEFAULT_STRING_LENGTH = 40;
    private static int TEN_BILLION = 10000000;

    public static void main(String[] args) {
        UsualReaderWriterTest();
        BufferedReaderWriterTest();
    }
    private static void UsualReaderWriterTest(){
        try {
            Writer usualWriter = new FileWriter(DEFAULT_FILE_NAME);
            Reader usualReader = new FileReader(DEFAULT_FILE_NAME);
            char buf='@';
            LOG.info("Start writing using usual writer!");
            long start = System.nanoTime();
            for(int k=0; k<TEN_BILLION; k++){
                usualWriter.write(StringGenerator.getRandomString(DEFAULT_STRING_LENGTH));
            }
            long finish = System.nanoTime();
            LOG.info("Writing finished. Total time: "+((double)(finish-start)/1000000000)+" seconds");
            LOG.info("Start reading billion characters using usual reader");
            start=System.nanoTime();
            for(int k=0; k<TEN_BILLION; k++){
                usualReader.read(CharBuffer.allocate(buf));
            }
            finish = System.nanoTime();
            LOG.info("Reading finished. Total time: "+((double)(finish-start)/1000000000)+" seconds");
            usualWriter.close();
            usualReader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    private static void BufferedReaderWriterTest(){
        try {
            Writer bufferedWriter = new BufferedWriter(new FileWriter(DEFAULT_FILE_NAME),DEFAULT_BUFFER_SIZE);
            Reader bufferedReader = new BufferedReader(new FileReader(DEFAULT_FILE_NAME),DEFAULT_BUFFER_SIZE);
            char buf='@';
            LOG.info("Start writing using Buffered writer!");
            long start = System.nanoTime();
            for(int k=0; k<TEN_BILLION; k++){
                bufferedWriter.write(StringGenerator.getRandomString(DEFAULT_STRING_LENGTH));
            }
            long finish = System.nanoTime();
            LOG.info("Writing finished. Total time: "+((double)(finish-start)/1000000000)+" seconds");
            LOG.info("Start reading billion characters using Buffered reader");
            start=System.nanoTime();
            for(int k=0; k<TEN_BILLION; k++){
                bufferedReader.read(CharBuffer.allocate(buf));
            }
            finish = System.nanoTime();
            LOG.info("Reading finished. Total time: "+((double)(finish-start)/1000000000)+" seconds");
            bufferedWriter.close();
            bufferedReader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
